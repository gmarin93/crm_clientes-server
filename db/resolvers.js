const Usuario = require("../models/Usuario");
const Cliente = require("../models/Cliente");
const Producto = require("../models/Producto");
const Pedido = require("../models/Pedido");
const bcryptjs = require('bcryptjs');
require('dotenv').config({ path: 'variables.env' });
const jwt = require('jsonwebtoken');


const crearToken = (usuario, secreta, expiresIn) => {

    const { id, email, nombre, apellido } = usuario;

    return jwt.sign({ id, email, nombre, apellido }, secreta, { expiresIn });
}

//La palabra secreta se requiere para obtener el usuario del token

//Resolvers
const resolvers = {
    Query: {
        obtenerUsuario: async (_, { token }) => {
            const usuarioId = await jwt.verify(token, process.env.SECRETA);

            return usuarioId;
        },

        obtenerProductos: async () => {
            try {
                const productos = await Producto.find({});

                return productos;

            } catch (error) {
                console.log(error);
            }
        },
        obtenerProducto: async (_, { id }) => {
            //revisar que existe el producto
            const producto = await Producto.findById(id);

            if (!producto)
                throw new Error('Producto no encontrado');

            return producto;
        },
        obtenerClientes: async () => {
            try {
                const clientes = await Cliente.find({});

                return clientes;
            } catch (error) {

            }
        },
        obtenerClientesVendedor: async (_, { }, ctx) => {

            try {
                const clientes = await Cliente.find({ vendedor: ctx.usuarioId.id.toString() });

                return clientes;
            } catch (error) {

            }
        },
        obtenerCliente: async (_, { id }, ctx) => {
            //Revisar si el cliente existe
            const cliente = await Cliente.findById(id);

            if (!cliente)
                throw new Error('El cliente no existe');

            //Validar que se carguen los clientes a los vendedores correspondientes
            if (cliente.vendedor.toString() !== ctx.usuarioId.id)
                throw new Error('No tienes las credenciales');

            return cliente;

        },
        obtenerPedidos: async () => {
            try {
                const pedidos = await Pedido.find({});
                return pedidos;
            } catch (error) {
                console.log(error);
            }
        },

        obtenerPedidosVendedor: async (_, { }, ctx) => {

            try {
                const pedidos = await Pedido.find({ vendedor: ctx.usuarioId.id });
                return pedidos;
            } catch (error) {
                console.log(error);
            }
        },
        obtenerPedido: async (_, { id }, ctx) => {
            //Si el pedido existe
            const pedido = await Pedido.findById(id);
            console.log(pedido.vendedor);

            if (!pedido)
                throw new Error('No existe el pedido');

            //Solo quien lo creo puede verlo
            if (pedido.vendedor.toString() !== ctx.usuarioId.id)
                throw new Error('No tienes las credenciales');

            //Retornar el pedido
            return pedido;
        },
        obtenerPedidoEstado: async(_,{estado},ctx)=>{

            const pedidos = await Pedido.find({vendedor:ctx.usuarioId.id, estado});
            return pedidos;
        },
        mejoresClientes: async() =>{
            const clientes = await Pedido.aggregate([
                {$match: {estado:"COMPLETADO"}},
                {$group:{
                    _id:"$cliente",
                    total:{$sum:'$total'}
                }},
                {
                    $lookup:{
                        from:'clientes',
                        localField:'_id',
                        foreignField:'_id',
                        as:'cliente'
                    }
                },{
                    $sort:{total:-1} //Mayor primero, esto es un innerjoin en mongo
                }
            ]);

            return clientes;
        },
        mejoresVendedores: async () =>{
            const vendedores = await Pedido.aggregate([
                {
                    $match:{estado:"COMPLETADO"}},
                    {$group:{
                        _id:"$vendedor",
                        total:{$sum:'$total'}
                    }},
                    {
                        $lookup:{
                            from:"usuarios",
                            localField:'_id',
                            foreignField:'_id',
                            as:'vendedor'
                        }
                    },
                    {
                        $limit:10
                    },
                    {
                        $sort:{total:-1}
                    }
            ])

            return vendedores;
        },
        
        buscarProducto: async(_,{texto})=>{
            const productos = await Producto.find({$text:{$search:texto}}).limit(10);

            return productos;
        }
    },
    Mutation: {
        nuevoUsuario: async (_, { input }) => {

            const { email, password } = input;

            //Revisar si el usuario esta registrado
            const existeUsuario = await Usuario.findOne({ email });
            if (existeUsuario)
                throw new Error('El usuario ya esta registrado');

            //Hashear el password
            const salt = await bcryptjs.genSalt(10);
            input.password = await bcryptjs.hash(password, salt);

            //Guardar en BD
            try {
                const usuario = new Usuario(input);
                usuario.save();
                return usuario;

            } catch (error) {
                console.log(error);
            }
        },

        autenticarUsuario: async (_, { input }) => {

            console.log(input);
            const { email, password } = input

            //Si el usuario existe
            const existeUsuario = await Usuario.findOne({ email });

            if (!existeUsuario)
                throw new Error('El usuario no existe');

            //Password correcto
            const passwordCorrecto = await bcryptjs.compare(password, existeUsuario.password);

            if (!passwordCorrecto)
                throw new Error('El password no es correcto');

            //Crear Token
            return {
                token: crearToken(existeUsuario, process.env.SECRETA, '24h')
            }

        },

        nuevoProducto: async (_, { input }) => {


            try {
                const producto = new Producto(input);

                //Se almacena en BD
                const resultado = await producto.save();

                return resultado;

            } catch (error) {
                console.log(error);
            }
        },

        actualizarProducto: async (_, { id, input }) => {

            let producto = await Producto.findById(id);

            if (!producto)
                throw new Error('Producto no encontrado');

            //Guardar en BD, new: true retorna la info actualizada, si no el anterior
            producto = await Producto.findOneAndUpdate({ _id: id }, input, { new: true });

            return producto;

        },

        eliminarProducto: async (_, { id }) => {

            let producto = await Producto.findById(id);

            if (!producto)
                throw new Error('Producto no encontrado');

            try {

                await Producto.findOneAndDelete({ _id: id });

            } catch (error) {
                console.log(error);
            }

            return "Producto Eliminado";

        },

        nuevoCliente: async (_, { input }, ctx) => {

            const { email } = input;

            //Verificar si  ya existe
            const cliente = await Cliente.findOne({ email });

            if (cliente)
                throw new Error('El cliente ya esta registrado');

            try {
                //Guardar en BD
                const nuevoCliente = new Cliente(input);
                //Asignar el vendedor
                nuevoCliente.vendedor = ctx.usuarioId.id

                const resultado = await nuevoCliente.save();

                return resultado;

            } catch (error) {
                console.log(error);
            }

        },

        actualizarCliente: async (_, { id, input }, ctx) => {
            //Verificar si existe
            let cliente = await Cliente.findById(id);

            if (!cliente)
                throw new Error('No existe el cliente');

            //Verificar si el vendedor es quien edita
            if (cliente.vendedor.toString() !== ctx.usuarioId.id)
                throw new Error('No tienes las credenciales');

            //Guardar cliente
            cliente = await Cliente.findOneAndUpdate({ _id: id }, input, { new: true });

            return cliente;
        },

        eliminarCliente: async (_, { id }, ctx) => {

            //Verificar si existe
            let cliente = await Cliente.findById(id);

            if (!cliente)
                throw new Error('No existe el cliente');

            //Verificar si el vendedor es quien edita
            if (cliente.vendedor.toString() !== ctx.usuarioId.id)
                throw new Error('No tienes las credenciales');

            //Eliminar cliente
            await Cliente.findOneAndDelete({ _id: id });

            return "Cliente eliminado";
        },

        nuevoPedido: async (_, { input }, ctx) => {

            const { cliente } = input;
            console.log(input);

            //Verificar si existe
            let clienteExiste = await Cliente.findById(cliente);

            if (!clienteExiste)
                throw new Error('No existe el cliente');

            //Verificar si el cliente pertenece al vendedor
            if (clienteExiste.vendedor.toString() !== ctx.usuarioId.id)
                throw new Error('No tienes las credenciales');

            //Verificar que el stock este disponible. For await es un loop que se ejecuta de forma asincrona
            for await (const articulo of input.pedido) {
                const { id } = articulo;

                const producto = await Producto.findById(id);

                if (articulo.cantidad > producto.existencia)
                    throw new Error(`El articulo: ${producto.nombre} excede la cantidad disponible`);
                else {
                    producto.existencia = producto.existencia - articulo.cantidad;
                    //Se actualiza el consumo de producto en BD
                    await producto.save();
                }
            }

            //Crear un nuevo pedido
            const nuevoPedido = new Pedido(input);

            //Asignarle un vendedor.
            nuevoPedido.vendedor = ctx.usuarioId.id

            //Guardar en la BD
            const resultado = await nuevoPedido.save();

            return resultado;
        },

        actualizarPedido: async (_, { id, input }, ctx) => {

            const {cliente} = input;

            console.log(input);

            //Verificar que existe el pedido
            const existePedido = await Pedido.findById(id);

            if (!existePedido)
                throw new Error('No tienes las credenciales');

            //Verificar que el cliente existe
            let clienteExiste = await Cliente.findById(cliente);

            if (!clienteExiste)
                throw new Error('No existe el cliente');

            //Verificar si el cliente el pedido pertenecen al vendedor
            if (clienteExiste.vendedor.toString() !== ctx.usuarioId.id)
                throw new Error('No tienes las credenciales');

            //Revisar el stock
            if(input.pedido){

                for await (const articulo of input.pedido) {
                    const { id } = articulo;
    
                    const producto = await Producto.findById(id);
    
                    if (articulo.cantidad > producto.existencia)
                        throw new Error(`El articulo: ${producto.nombre} excede la cantidad disponible`);
                    else {
                        producto.existencia = producto.existencia - articulo.cantidad;
                        //Se actualiza el consumo de producto en BD
                        await producto.save();
                    }
                }
            }

            //Guardar el pedido
            const resultado = await Pedido.findOneAndUpdate({_id:id},input,{new:true});
            return resultado;
        },
        eliminarPedido: async(_,{id},ctx)=>{
            //Verificamos si el pedido existe
            const pedido = await Pedido.findById(id);

            if(!pedido){
                throw new Error('El pedido no existe');
            }

            //Verificar si es el vendedor que va a eliminarlo
            if(pedido.vendedor.toString() !== ctx.usuarioId.id)
                throw new Error('No tienes las credenciasles');

            //Eliminar el pedido
            await Pedido.findOneAndDelete({_id:id});
            return "Pedido Eliminado";
        }

    }
};

module.exports = resolvers;