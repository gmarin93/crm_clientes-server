const mongoose = require('mongoose');

const ProductoSchema= mongoose.Schema({

    nombre:{
        type:String,
        trim:true
    },
    pedido:{
        type:Array,
        required:true
    },
    cliente:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'Cliente',
        required:true,
    },
    vendedor:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'Usuario',
        required:true,
    },
    total:{
        type:Number,
        required:true
    },
    estado:{
        type:String,
        required:true,
        trim:true,
        default:'PENDIENTE'
    },
    creado:{
        type:Date,
        default:Date.now()
    }
});

module.exports = mongoose.model('Pedido',ProductoSchema);