const mongoose = require('mongoose');

const ProductosSchema= mongoose.Schema({

    nombre:{
        type:String,
        required:true,
        trim:true
    },
    existencia:{
        type:Number,
        required:true,
        trim:true
    },
    precio:{
        type:Number,
        required:true,
        trim:true
    },
    creado:{
        type:Date,
        default:Date.now()
    }
});

ProductosSchema.index({nombre:'text'}); //Realiza una busqueda rapida, como Amazon cuando se busca un producto, rapidamente commo un texto a este atributo de la tabla

module.exports = mongoose.model('Producto',ProductosSchema);