const mongoose = require('mongoose');
require('dotenv').config({path:'variables.env'});

const conectarDB = async () =>{
 
    try {
        
        await mongoose.connect(process.env.DB_MONGO,{
            useNewUrlParser:true,
            useUnifiedTopology:true,
            useFindAndModify:true,
            useCreateIndex:true
        });
        console.log('DB Conectada')
    } catch (error) {
        console.log('Error conectando a DB: '+error);
        process.exit(1); //Finaliza la aplicacion inmediatamente
    }
}

module.exports = conectarDB;