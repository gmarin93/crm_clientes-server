const { ApolloServer} =  require('apollo-server');
const typeDefs = require('./db/schema');
const resolvers = require('./db/resolvers');
const jwt = require('jsonwebtoken');
const conectarDB = require('./config/db');
require('dotenv').config({path:'variables.env'});


//Conectar a DB
conectarDB();

//servidor
const server = new ApolloServer({
    typeDefs,
    resolvers,
    context: ({req}) =>{

        const token = req.headers['authorization'] || '';

        if(token){
            try {

                const usuarioId = jwt.verify(token.replace('Bearer',''),process.env.SECRETA);

                return{
                    usuarioId
                }
            } catch (error) {
                console.log('Hubo un error '+error);
            }
        }
    }
});

//arrancar el servidor
server.listen().then(({url})=>{
    console.log(`Servidor listo en la URL ${url}`);
});

